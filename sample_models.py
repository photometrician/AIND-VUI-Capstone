from keras import backend as K
from keras.models import Model
from keras.layers import (BatchNormalization, Conv1D, Dense, Input,
                          TimeDistributed, Activation, Bidirectional, SimpleRNN, GRU, LSTM)


def simple_rnn_model(input_dim, output_dim=29):
    """ Build a recurrent network for speech 
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # Add recurrent layer
    simp_rnn = GRU(output_dim, return_sequences=True,
                   implementation=2, name='rnn')(input_data)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(simp_rnn)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: x
    print(model.summary())
    return model


def rnn_model(input_dim, units, activation, output_dim=29):
    """ Build a recurrent network for speech 
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # Add recurrent layer
    simp_rnn = GRU(units, activation=activation,
                   return_sequences=True, implementation=2, name='rnn')(input_data)
    # TODO: Add batch normalization 
    bn_rnn = BatchNormalization(name='bn_rnn')(simp_rnn)
    # TODO: Add a TimeDistributed(Dense(output_dim)) layer
    time_dense = TimeDistributed(Dense(output_dim))(bn_rnn)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: x
    print(model.summary())
    return model


def cnn_rnn_model(input_dim, filters, kernel_size, conv_stride,
                  conv_border_mode, units, output_dim=29):
    """ Build a recurrent + convolutional network for speech 
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # Add convolutional layer
    conv_1d = Conv1D(filters, kernel_size,
                     strides=conv_stride,
                     padding=conv_border_mode,
                     activation='relu',
                     name='conv1d')(input_data)
    # Add batch normalization
    bn_cnn = BatchNormalization(name='bn_conv_1d')(conv_1d)
    # Add a recurrent layer
    simp_rnn = SimpleRNN(units, activation='relu',
                         return_sequences=True, implementation=2, name='rnn')(bn_cnn)
    # TODO: Add batch normalization
    bn_rnn = BatchNormalization(name='bn_rnn')(simp_rnn)
    # TODO: Add a TimeDistributed(Dense(output_dim)) layer
    time_dense = TimeDistributed(Dense(output_dim))(bn_rnn)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: cnn_output_length(
        x, kernel_size, conv_border_mode, conv_stride)
    print(model.summary())
    return model


def cnn_output_length(input_length, filter_size, border_mode, stride,
                      dilation=1):
    """ Compute the length of the output sequence after 1D convolution along
        time. Note that this function is in line with the function used in
        Convolution1D class from Keras.
    Params:
        input_length (int): Length of the input sequence.
        filter_size (int): Width of the convolution kernel.
        border_mode (str): Only support `same`, `valid` or 'causal'.
        stride (int): Stride size used in 1D convolution.
        dilation (int)
    """
    if input_length is None:
        return None
    assert border_mode in {'same', 'valid', 'causal'}
    dilated_filter_size = filter_size + (filter_size - 1) * (dilation - 1)
    if border_mode == 'same':
        output_length = input_length
    elif border_mode == 'valid':
        output_length = input_length - dilated_filter_size + 1
    elif border_mode == 'causal':
        # as code below, causal mode's output_length is input_length.
        # https://github.com/fchollet/keras/blob/master/keras/utils/conv_utils.py#L112
        output_length = input_length
    return (output_length + stride - 1) // stride


def deep_rnn_model(input_dim, units, recur_layers, output_dim=29):
    """ Build a deep recurrent network for speech 
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # TODO: Add recurrent layers, each with batch normalization
    bn_rnn = input_data
    for i in range(recur_layers):
        # Add recurrent layer
        simp_rnn = GRU(units, activation='relu',
                       return_sequences=True, implementation=2,
                       name="rnn"+str(i))(bn_rnn)
        bn_rnn = BatchNormalization(name="bn_rnn"+str(i))(simp_rnn)

    # TODO: Add a TimeDistributed(Dense(output_dim)) layer
    time_dense = TimeDistributed(Dense(output_dim))(bn_rnn)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: x
    print(model.summary())
    return model


def bidirectional_rnn_model(input_dim, units, output_dim=29):
    """ Build a bidirectional recurrent network for speech
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # TODO: Add bidirectional recurrent layer
    bidir_rnn = Bidirectional(GRU(units,
                                  activation='relu',
                                  return_sequences=True,
                                  implementation=2,
                                  name='rnn'))(input_data)
    # TODO: Add a TimeDistributed(Dense(output_dim)) layer
    time_dense = TimeDistributed(Dense(output_dim))(bidir_rnn)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: x
    print(model.summary())
    return model


def deep_bidirectional_rnn_model(input_dim, units, recur_layers, output_dim=29):
    """ Build a deep bidirectional recurrent network for speech
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # https://arxiv.org/pdf/1510.01378.pdf
    # According to the batch normalization paper above,
    # it is recommended to apply batch normalization only to the input-to-hidden transition for RNN
    # in terms of training cost.
    # And as the result of my experiment, I adopted it.
    bidir_rnn = BatchNormalization(name="bn_rnn1")(input_data)
    for i in range(recur_layers):
        bidir_rnn = Bidirectional(GRU(units,
                                      activation='relu',
                                      return_sequences=True,
                                      implementation=2,
                                      name="rnn"+str(i)))(bidir_rnn)
    bidir_rnn = BatchNormalization(name="bn_rnn2")(bidir_rnn)
    time_dense = TimeDistributed(Dense(output_dim))(bidir_rnn)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: x
    print(model.summary())
    return model


def rnn_with_dropout_model(input_dim, units, activation, dropout, recurrent_dropout, output_dim=29):
    """ Build a recurrent network with dropout parameters for speech
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # Add recurrent layer
    simp_rnn = GRU(units, activation=activation,
                   return_sequences=True, implementation=2, name='rnn',
                   dropout=dropout, recurrent_dropout=recurrent_dropout)(input_data)

    # TODO: Add batch normalization
    bn_rnn = BatchNormalization(name='bn_rnn')(simp_rnn)
    # TODO: Add a TimeDistributed(Dense(output_dim)) layer
    time_dense = TimeDistributed(Dense(output_dim))(bn_rnn)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: x
    print(model.summary())
    return model


def dilated_cnn_rnn_model(input_dim, filters, kernel_size,
                          units, conv_layers, output_dim=29):
    """ Build a recurrent + dilated convolutional network for speech
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # Add causal convolutional layer
    conv_1d = input_data
    # According to paper, dilation_rage set layer depth ** 2
    # https://arxiv.org/pdf/1609.03499.pdf
    for i in range(conv_layers):
        conv_1d = Conv1D(filters, kernel_size,
                         padding='causal',
                         activation='relu',
                         dilation_rate=2**i,
                         name="conv1d"+str(i))(conv_1d)
        # Add batch normalization
        conv_1d = BatchNormalization(name="bn_conv_1d"+str(i))(conv_1d)
    # Add a recurrent layer
    simp_rnn = SimpleRNN(units, activation='relu',
                         return_sequences=True, implementation=2, name='rnn')(conv_1d)
    # TODO: Add batch normalization
    bn_rnn = BatchNormalization(name='bn_rnn')(simp_rnn)
    # TODO: Add a TimeDistributed(Dense(output_dim)) layer
    time_dense = TimeDistributed(Dense(output_dim))(bn_rnn)
    # Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    model.output_length = lambda x: cnn_output_length(
        x, kernel_size, 'causal', 1)
    print(model.summary())
    return model


def final_model(input_dim=161, filters=200, kernel_size=11, units=200, conv_layers=2,
                recur_layers=2, dropout=0.1, recurrent_dropout=0.1, output_dim=29):
    """ Build a deep network for speech 
    """
    # Main acoustic input
    input_data = Input(name='the_input', shape=(None, input_dim))
    # TODO: Specify the layers in your network
    # Add causal convolutional layer
    conv_1d = input_data
    # According to paper, dilation_rate set layer depth ** 2
    # https://arxiv.org/pdf/1609.03499.pdf
    for i in range(conv_layers):
        conv_1d = Conv1D(filters, kernel_size,
                         padding='causal',
                         activation='relu',
                         dilation_rate=2**i,
                         name="conv1d"+str(i))(conv_1d)
        conv_1d = BatchNormalization(name="bn_conv1d"+str(i))(conv_1d)

    # add deep bidirectional layer
    bidir_rnn = BatchNormalization(name="bn_rnn1")(conv_1d)
    for i in range(recur_layers):
        bidir_rnn = Bidirectional(GRU(units,
                                      activation='relu',
                                      return_sequences=True,
                                      implementation=2,
                                      dropout=dropout,
                                      recurrent_dropout=recurrent_dropout,
                                      name="rnn"+str(i)))(bidir_rnn)
    bidir_rnn = BatchNormalization(name="bn_rnn2")(bidir_rnn)
    time_dense = TimeDistributed(Dense(output_dim))(bidir_rnn)
    # TODO: Add softmax activation layer
    y_pred = Activation('softmax', name='softmax')(time_dense)
    # Specify the model
    model = Model(inputs=input_data, outputs=y_pred)
    # TODO: Specify model.output_length
    # use cnn_output_length, same as cnn_rnn_model
    model.output_length = lambda x: cnn_output_length(
        x, kernel_size, 'causal', 1)
    print(model.summary())
    return model
